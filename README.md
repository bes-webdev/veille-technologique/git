# GIT
## Qu'est ce que c'est ? 
### Selon Wikipedia :
Git est un [logiciel](https://fr.wikipedia.org/wiki/Logiciel_de_gestion_de_versions) de gestion de versions décentralisé. C'est un logiciel libre créé par Linus Torvalds, auteur du noyau Linux, et distribué selon les termes de la licence publique générale GNU version 2.

Dit comme ça, ça nous fait une belle jambe. Mais en cliquant sur le lien de la définition on se rend compte que :
1.  Git est un LOGICIEL. C'est donc un programme qui s'installe sur la machine sur laquelle on souhaite s'en servir (que cela soit un server ou un ordinateur de développement).
2.  Git est un logiciel de VERSIONS. Cela veut dire que son rôle est de garder en mémoire chaque modification concernant un ensemble de fichiers (que cela soit un ajout de fichier, une modification ou une suppression).
3.  Git est DECENTRALISE. Ca veut dire que qu'il suffit de cloner ou de forker un projet (repository) pour commencer à travailler dessus. Bien qu'il soit décentralisé on travaille en général avec  un repository central sur lequel on va héberger le projet. On peut soit créer un server prêt à accueillir Git, soit utiliser un service existant. Le plus connu est Github qui a l'avantage d'être gratuit, tout comme framagit (que l'on va utiliser dans ce cours). 

Ces trois éléments nous permettent d’appréhender le logiciel et de commencer à s'en servir.

## Comment ça marche ?
Il existe des interfaces graphiques pour Git (spécialement sous Windows), et nos IDE intègrent aussi Git. Mais pour bien comprendre le fonctionnement nous allons plutôt l'utiliser en mode console.
Imaginez un repository comme une ligne du temps. Chaque fois qu'on apporte une modification au projet (que l'on fait un commit), on fait avancer cette ligne. La tête de cette ligne est appelée HEAD. Il est possible a tout moment de revenir à « un moment précis du passé » (un commit donné), qui s'appelle une révision.

## Les branches
Le concept de git jusque là est assez sympa mais il y a beaucoup plus, notamment les branches. Voyez les branches  comme une ligne du temps parallèle qui a bifurqué du head à un moment donné.

Sur une branche, il est possible de faire toutes les modifications que l'on veut, sans impacter le HEAD. Pratique pour développer une feature sans déranger ceux qui travaillent sur autre chose ou sans risquer de casser le jouet. On peut fusionner ces branches plus tard avec le head, avec une autre branche, ou ne jamais rien faire de plus.
Pour le reste il y a encore beaucoup à dire mais nous en resterons là pour ce cours d'initiation.

![Emmet Brown](docBrown.jpg)

## Quelques commandes
*  `git init` : sert à créer un nouveau repository. Il peut être utilisé pour convertir un projet existant en repository git ou pour créer un repository vide. La plupart des commandes git sont indisponibles en dehors d'un repository initialisé. C'est donc souvent la première commande que l'on va exécuter.
*  `git clone <repo>` : sert à cloner un repository existant dans le dossier courant. Le repository existant peut être un repository local ou un repository distant, accessible via http ou via ssh.
*  `git add <file>` : sert à ajouter au repository un fichier présent dans le dossier courant. Il sert donc à dire que le fichier est donc maintenant versionné.
*  `git commit <file>` : sert à enregistrer une modification dans le repository.
*  `git push` : sert à envoyer les différents commits dans la branche courante. Tant que cette commande n'est pas exécutée, les commits ne sont valables que localement.
*  `git pull --rebase`: sert à mettre le repository à jour avec la branche courante.
*  `git checkout <commit> <file>` : sert à remettre un fichier (si file est précisé, sinon l'ensemble du projet) dans l'état ou il était au moment de tel commit sans toucher au HEAD. On s'en sert généralement dans un but de consultation. Pour voir dans quel état était le fichier à ce moment. git checkout master remet le tout dans l'état correspondant au à la branche courante. 
*  `git revert <commit>` : sert à remettre le repository dans l'état ou il était au moment de tel commit. Avec la nuance qu'il modifie les fichiers pour les remettre en état, puis crée un nouveau commit avec cet état. L'avantage c'est qu'on ne perd pas ses modifications.

Branches

*  `git branch` : sert à lister toutes les branches d'un repository.
*  `git branch <branch>`: sert à créer une nouvelle branche, sans rien faire dessus. Avec -d il supprime la branche, avec -m il renomme la branche existante en <branch>.
*  `git checkout <branch>` : sert à mettre à jour le projet courant pour correspondre à une branche existante. C'est une manière de naviguer entre les branches. Avec -b, il crée la nouvelle branche <branch> et fais un checkout dessus. C'est à dire qu'il va synchroniser cette branche avec le contenu du dossier.
*  `git merge <branch>` : sert à fusionner la branche <branch> dans la branche courante. Git s’occupe de tout.

## Les conflits
Inévitablement, en utilisant git on tombe sur des conflits. Deux personnes (ou une personne sur deux postes différents) ont modifié un fichier, et git ne peut pas merger tout seul ces modifications. Au moment du pull, git nous prévient que certains commits sont mis en conflit. Met en quarantaine ces fichiers et nous demande de régler ces conflits. 
Dans ce cas il suffit d'éditer ces fichiers pour décider de l'état dans lequel ils doivent être (faire un merge manuellement) et dire à git que le conflit est résolu avec la commande git add <some-file> git rebase –continue . C'est tout.

## Conclusion
Git est doucement devenu incontournable dans l'univers des developpers web. Les raisons sont claires et évidentes. 
Le fait de versionner son projet et de gérer la concurrence entre les membres de l'équipe de développement (le fait que plusieurs personnes voudraient modifier le même fichier au même moment) est clairement nécessaire.
Git a aussi fait oublier les clients FTP comme filezilla à de nombreux developpers. Il suffit maintenant de se connecter au server en SSH (protocole de connexion à distance en lignes de commande)  et de lancer la commande git pull pour le mettre à jour. C'est beaucoup plus rapide.
Le fait d'avoir un repository central cloné sur plusieurs machines fait que l’entièreté du projet (hors DB) est backupé dans toutes ses versions (en tous cas jusqu'au dernier pull de la machine la plus à jour). En cas de crash on trouve toujours bien un clone du projet sur lequel repartir (en général on repart du repository central).
Il y a encore énormément à dire sur git: ce logiciel offre beaucoup de fonctionnalités. Vous en savez assez pour vous en servir gentillement, mais si vous voulez en savoir plus je vous invite à consulter ce tutoriel (https://www.atlassian.com/git/tutorials/setting-up-a-repository) qui m'a bien servi dans la rédaction de ce cours. La documentation (http://git-scm.com/doc) est évidemment disponible et pourrait vous être d'une grande aide les jours où vous serez bloqués.
